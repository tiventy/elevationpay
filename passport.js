const passport = require('passport');
const facebook = require('passport-facebook').Strategy;
const jwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const facebookstratetegy = require('passport-facebook-token');
const Users = require('./api/models/users');
const mongoose = require('mongoose');


passport.use(new jwtStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: process.env.JWT_TOKEN

}, async (payload,done) =>{
    try {
const  user = Users.findById(payload.userId)

        if (!user){
    return done(null,false);
        }

        done(null,user);
    }
catch(error){
        done(error,false);
};
    }
));

passport.use('facebook',new facebookstratetegy(
    {
        clientID:
        process.env.clientId,
        clientSecret: process.env.clientSecret
    }, async (accessToken,refreshToken,profile,done) =>
    {
try {
    console.log('accessToken', accessToken)
    console.log('refreshToken', refreshToken)
    console.log('profile', profile)

  //   const existinguser = await Users.findOne({"facebookid " : profile.id});
  //   if (existinguser){
  //       return done(existinguser,null)
  //   }
  //   var newuser = new Users({
  //       method: "facebook",
  //       facebook: {
  //           id: profile.id,
  //           email: profile.emails[0].value
  //       }
  //   });
  //
  // newuser.save();
  // done(null,newuser);
    Users.findOne({facebook: profile.id}, function (err,user) {

        if (err) return res.status(401).json({
            message: err
        }) ;

        if (user) return done(null,user);

        else {
            var NewUser = new Users();
            NewUser.email = profile._json.email;
            NewUser.facebook = profile.id;
            //NewUser.tokens.push({kind: 'facebook', token : token}) ;
            NewUser.name = profile.displayName;

            NewUser.save(function (err) {
                if (err) throw err;
                return done(NewUser);


            })


        }

    })

}
catch (error) {
    done(error,false, error.message)
    
}

}));