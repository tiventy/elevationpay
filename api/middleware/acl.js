
module.exports.onlyUserWithEmail = (email)=>{
    return (req,res, next)=>{
        if(!req.Userdata)
            return res.status(403)
                .json({message: "authentication failed."});

        if(req.Userdata.email === email)
            return next();
        return res.status(401).json({message:""});
    };
};


module.exports.ensureRole = (item)=>{
    let roles = (item || '').replace(/\s+/g, ',').split(',').map(x => x.toLowerCase()).filter(x=>!!x);
    return (req,res, next)=>{
        if(!req.Userdata)
            return res.status(403)
                .json({message: "authentication failed."});
        if(!roles.length) return next();
        for (let i = 0; i <= roles.length - 1; i++) {
            if ((req.Userdata.roles||[]).indexOf(roles[i]) > -1)
                return next();
        }

        return res.status(401).json({message:"Not allowed to access this resource"});
    };
};
