const mongoose = require('mongoose');


const subscriptionSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    //User:{type: String, ref: 'Users', required: true },
    customer:{type: String, required: true},
    plan: {type: String, required: true},
    status: {type: String, required: true},
    amount: {type: String, required: true},
    authorization: {type: String, required: true},
    subscription_code: {type: String, required: true},
    id: {type: String, required: true},
    createdAt: {type: String, required: true},
    email_token: {type: String, required: true},

    created: {
        type: Date,
        default: Date.now
    }



});

module.exports = mongoose.model('subscription', subscriptionSchema);