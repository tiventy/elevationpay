const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const userSchema = mongoose.Schema({
    //_id: mongoose.Schema.Types.ObjectId,
    name:{type: String, required: true},
    email:{type: String, required: true, unique: true,
    },
    token : Array,
    password:{type: String, required: true},
    phone:{type: String},
    created: {
        type: Date,
        default: Date.now
    },
    active:{type : Boolean, required: true, default: false},
    temporarytoken :{ required: true, type: String},
    resettoken:{required : false, type: String},
    roles:Array
});

const hashPassword = (password, callback) => {
    bcrypt.genSalt(10, (err, salt) => {
        if (err) return callback(err);
        bcrypt.hash(password, salt, null, callback);
    });
};
userSchema.statics.hashPassword = hashPassword;

userSchema.pre('save', function (next) {
    let self = this;
//if (!self.isModified('password')) return next();
    if(!self.roles) self.roles = ["user"];
    else if(self.roles.indexOf("user") === -1) self.roles.push("users");

    if (this.isModified("password"))
        hashPassword(this.password, (e, p) => {
            self.password = p;
            next();
        });
    else next();
});
let config = {
    transform: function (doc, ret) {
        delete ret.password;
        delete ret.token;
        delete ret["__v"];
        return ret;
    }
};
userSchema.set('toJSON', config);
userSchema.set('toObject', config);
module.exports = mongoose.model('Users', userSchema);