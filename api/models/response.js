const mongoose = require('mongoose');


const responseSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    //User:{type: mongoose.Schema.Types.ObjectId, ref: 'Users', required: true },
    reference:{type: String, required: true},
    status: {type: String, required: true},
    chargestatus: {type: String, required: true},
    amount: {type: String, required: true},
    time: {type: String, required: true},
    authorizationcode: {type: String, required: true},
    lastdigit: {type: String, required: true},
    exp_month: {type: String, required: true},
    exp_year: {type: String, required: true},
    card_type: {type: String, required: true},
    email: {type: String, required: true},
    customerid: {type: String, required: true},



    created: {
        type: Date,
        default: Date.now
    }



});

module.exports = mongoose.model('response', responseSchema);