const mongoose = require('mongoose');


const planSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    User:{type: String, ref: 'Users', required: true },
    status:{type: String, required: true},
    amount: {type: String, required: true},
    name: {type: String, required: true},
    interval: {type: String, required: true},
    currency: {type: String, required: true},
    plan_code: {type: String, required: true},
    createdAt: {type: String, required: true},
    id: {type: String, required: true},

    created: {
        type: Date,
        default: Date.now
    }



});

module.exports = mongoose.model('plan', planSchema);