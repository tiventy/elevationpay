const mongoose = require('mongoose');



const detailsSchema = mongoose.Schema({
    //_id: mongoose.Schema.Types.ObjectId,
    User:{type: mongoose.Schema.Types.ObjectId, ref: 'Users', required: true },
    amount:{type: String, required: true},
    email: {type: String, required: true},
    purpose: {type: String, required: true},
    //subaccount:{type:String, required:true,enum:['ACCT_6b51y79rq8af5sy']},
        //enum:["ACCT_6b51y79rq8af5sy"]},
//zenith - ACCT_6b51y79rq8af5sy
    //reference: {type: String, required: false},
    created: {
        type: Date,
        default: Date.now
    }


});


module.exports = mongoose.model('details', detailsSchema);


