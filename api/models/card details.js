const mongoose = require('mongoose');


const carddetailsSchema = mongoose.Schema({
    //_id: mongoose.Schema.Types.ObjectId,
    //User:{type: mongoose.Schema.Types.ObjectId, ref: 'Users', required: true },
    lastdigit:{type: String, required: true},
    exp_month: {type: String, required: true},
    exp_year: {type: String, required: true},
    card_type: {type: String, required: true},
    email: {type: String, required: true},
    customerid: {type: String, required: true},
    authorizationcode: {type: String, required: true},
    //subaccount:{type:String, required:true,enum:['ACCT_6b51y79rq8af5sy']},


    //reference: {type: String, required: false},
    created: {
        type: Date,
        default: Date.now
    }


});

module.exports = mongoose.model('carddetails', carddetailsSchema);