const request = require('request-promise');
var crypto = require('crypto');
var Response = require('../models/response');
var Details = require('../models/details');
var carddetails = require('../models/card details');

const Users = require('../models/users');
var nodemailer = require('nodemailer');
var Plan = require('../models/plans');
var Subscription = require('../models/subscription');
const mongoose = require('mongoose');



exports.checkuser_card = (req, res) => {
    var id = req.Userdata.email;
    carddetails.find({email: id}).exec().then(doc => {
        req.userRes = doc[0];
        var check = req.userRes.lastdigit;
        var lastdig = req.body.lastdigit

        if (lastdig === '') {
            res.status(500).json({
                ResponseCode: "500",
                message: 'please input card details'
            })
        }
        if (lastdig != check) {
            res.status(500).json({
                ResponseCode: "500",
                message: 'card details does not exist'
            })

        }
        else {
            carddetails.find({lastdigit: lastdig}).exec().then(card => {
                res.status(200).json({
                    ResponseCode: "200",
                    message: "card exist"
                })
            })
                .catch(err => {
                    console.log(err);
                    res.status(500).json({
                        ResponseCode: "500",
                        message: err
                    })
                })
        }
    })
}


exports.get_cards = (req, res) => {

    var id = req.Userdata.email;

    // console.log(id);

    carddetails.find({email: id}).select('exp_month exp_year card_type lastdigit authorizationcode').exec().then(doc => {
        req.userRes = doc[0];

        res.status(200).json({
            ResponseCode: "200",
            doc: doc

        })
    }).catch(err => {
        res.status(500).json({
            ResponseCode: "500",
            message: err
        })
    });
}


exports.delete_card = (req, res) => {
    var id = req.Userdata.email;

    carddetails.find({email: id}).exec().then(doc => {
        req.userRes = doc[0];
        var check = req.userRes.lastdigit;
        var lastdig = req.body.las;

        if (lastdig === '') {
            res.status(500).json({
                ResponseCode: "500",
                message: 'please input card details'
            })
        }
        if (lastdig != check) {
            res.status(500).json({
                ResponseCode: "500",
                message: 'card details does not exist'
            })

        }

        else {

            carddetails.deleteOne({lastdigit: req.body.las}).exec().then(card => {
                res.status(200).json({
                    ResponseCode: "200",
                    message: "card deleted"
                })
            }).catch(err => {
                console.log(err);
                res.status(500).json({
                    ResponseCode: "500",
                    message: err
                })
            })
        }
    });
}


exports.Disable_Subscription = (req, res) => {

    var id = req.Userdata.email;
    Subscription.find({customer: id}).then(user => {
        req.userRes = user[0];
        console.log(req.userRes.subscription_code)
        console.log(req.userRes.email_token)


        var options2 = {
            url: "https://api.paystack.co/subscription/disable",
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer sk_test_da1fd67380a1213f2bf1c9e09d4a033744eba0d8'
            },
            body: {
                "token": req.userRes.email_token,
                "code": req.userRes.subscription_code,
                //"authorization": req.userResponse.authorizationcode
            },
            json: true
        }
        request(options2)
            .then(answer => {
                console.log(answer)
                res.send(answer)
            }).catch(function (err) {
            console.log(err)

        })
    })
}


exports.create_plan = (req, res) => {
    // var ode = req.Response.authorization_code
    // console.log("this is " + ode)
    var options = {
        url: "https://api.paystack.co/plan",
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer sk_test_da1fd67380a1213f2bf1c9e09d4a033744eba0d8'
        },
        body: {


            "amount": req.body.amount * 100,
            "name": req.body.name,
            "interval": req.body.interval
        },
        json: true
    }

    request(options)
        .then(
            function (response) {
                console.log(response)
                res.status(200).json({
                    ResponseCode: "200",
                    Message: "plan created" });

                if (response.message === 'Plan created' ) {

                    exports.Subscribe(req, res);
                    return res.status(200);

                    const plan = new Plan({
                        _id: new mongoose.Types.ObjectId(),
                        User: req.Userdata.email,
                        status: response.status,
                        amount: response.data.amount / 100,
                        name: response.data.name,
                        interval: response.data.interval,
                        currency: response.data.currency,
                        plan_code: response.data.plan_code,
                        createdAt: response.data.createdAt,
                        id: response.data.id
                    })
                    plan.save().then(result => {

                        console.log(result)
                    }).catch(err =>
                        console.log(err));


                }
else{
                    res.status(500).json({
                    ResponseCode:500,
                    Message: "Plan not created"
                })}
            })
        .catch(function (err) {
            console.log(err)

        })
}

exports.Subscribe = (req, res) => {
    var id = req.Userdata.email;
    Plan.find({User: id}).then(user => {
        req.userRes = user[0];
        console.log(req.userRes.plan_code);


        var options2 = {
            url: "https://api.paystack.co/subscription",
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer sk_test_da1fd67380a1213f2bf1c9e09d4a033744eba0d8'
            },
            body: {
                "customer": id,
                "plan": req.userRes.plan_code,
                //"authorization": req.userResponse.authorizationcode
            },
            json: true
        }

        request(options2)
            .then(
                function (answer) {
                    console.log(answer)
                    res.send(answer)
                    //if (answer.status === 'true'){

                    const subscribe = new Subscription({
                        _id: new mongoose.Types.ObjectId(),
                        //User:id,
                        customer: id,
                        plan: answer.data.plan,
                        status: answer.status,
                        amount: answer.data.amount,
                        authorization: answer.data.authorization,
                        subscription_code: answer.data.subscription_code,
                        id: answer.data.id,
                        email_token: answer.data.email_token,
                        createdAt: answer.data.createdAt

                    })
                    subscribe.save().then(result => {

                        console.log(result)
                    }).catch(err =>
                        console.log(err));


                    //}

                }).catch(function (err) {
            console.log(err)
            res.send(err)

        })
    })


}


exports.invite_friends = (req, res) => {

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'tiventyglobal@gmail.com',
            pass: '07032155156'
        }
    });


    const mailOptions = {
        from: 'hello@tiventy.com.com', // sender address
        to: req.body.email, // list of receivers
        subject: ' Elevation Pay', // Subject line
        html: ' Hello Dear ' + '<p>Have you checked out elevationpay app? it lets you send money to your church conviniently</p>'// plain text body
    };
    transporter.sendMail(mailOptions, function (err, info) {
        if (err)
            console.log(err)
        else
            console.log(info);
    });
    console.log(req.body.email);
    res.send("done " + req.body.email)

    // const accountSid = 'AC5b53382f89b0b1ac323edfa0b4d063d2';
    // const authToken = '1e4f591937e56660352cc46104035819';
    // const client = require('twilio')(accountSid, authToken);
    //
    // client.messages
    //     .create({from: '+18142574688', body: 'tiventy code is 00234', to: items.PhoneNumber})
    //     .then(message => console.log(message.sid))
    //     .done();
}

exports.get_transactions = (req, res, next) => {
    var id = req.Userdata.email;
    var mysort = { created: -1 };

    console.log(id);

    Response.find({email: id}).sort(mysort).exec().then(doc => {
        console.log(doc)
        res.status(200).json({
            ResponseCode: "200",
            doc: doc
        })
    }).catch(err => {
        res.status(500).json({
            ResponseCode: "500",
            message: err
        })
    });
}

exports.get_alltransactions = (req, res, next) => {
    var mysort = { created: -1 };

    Response.find().sort(mysort)
        .exec()
        .then(docs => {
            res.status(200).json(
                {
                    ResponseCode: "200",
                    count: docs.length,
                    user: docs
                        //.map(doc => {
                    //
                    //     return {
                    //
                    //         user: doc,
                    //         request: {
                    //             type: 'GET',
                    //             url: req.hostUrl + '/details/getalltransactions/' + doc._id
                    //         }
                    //     }
                    // }
                    //),
                })
        }).catch(err => {
        res.status(500).json({
            ResponseCode: "500",
            message: err
        });
    });

}
exports.pay_api = (req, res) => {
    var id = req.Userdata.email;
    //console.log(id);
    Response.find({email: id}).exec().then(user => {
        // if (user.length >= 1) {
        //     req.userResponse = user[0];
        //     //console.log("kiopl " + user[0]);
        //     //exports.post_authorized(req,res);
        //     return res.status(200)
        // }
        const details = new Details({
            _id: new mongoose.Types.ObjectId(),
            User: req.Userdata.userId,
            amount: req.body.amount,
            email: req.Userdata.email,
            purpose: req.body.purpose,
            subaccount: req.body.subaccount
        })
        details
            .save()
            .then(result => {
                console.log(result);
                console.log("saved details");
            })
            .catch(err =>
                console.log(err));

        var options = {
            url: "https://api.paystack.co/transaction/initialize",
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer sk_test_da1fd67380a1213f2bf1c9e09d4a033744eba0d8'
            },
            body: {
                "callback_url": `${req.headers['x-forwarded-proto'] || req.protocol}://${req.get('host')}/details/callback`,
                "amount": details.amount * 100,
                "email": details.email,
                "subaccount": details.subaccount
            },
            json: true
        }

        request(options)
            .then(
                function (response) {
                    console.log(response)
                    res.send(response)
                    //res.redirect(response.data.authorization_url);
                })
            .catch(function (err) {
                console.log(err)

            })
        ;
    })

}


exports.add_newcard = (req, res) => {
    var id = req.Userdata.email;
    //console.log(id);
    const details = new Details({
        _id: new mongoose.Types.ObjectId(),
        User: req.Userdata.userId,
        amount: req.body.amount,
        email: req.Userdata.email,
        purpose: req.body.purpose
    })
    details
        .save()
        .then(result => {
            //console.log(result);
            console.log("DONE");
        })
        .catch(err =>
            console.log(err));

    res.status(200).json

    var options = {
        url: "https://api.paystack.co/transaction/initialize",
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer sk_test_da1fd67380a1213f2bf1c9e09d4a033744eba0d8'
        },
        body: {

            "amount": details.amount * 100,
            "email": details.email,
        },
        json: true
    }

    request(options)
        .then(
            function (response) {
                console.log(response)
                res.redirect(response.data.authorization_url);
            })
        .catch(function (err) {
            console.log(err)

        });

}


// var urljoin = require('url-join');
// var url = 'https://api.paystack.co/transaction/verify/9j54whin4s';
//
// var fullurl = urljoin(url);
//  console.log("check" + fullurl)


exports.get_payapi = function (req, res) {
    request.get({
        url: fullurl,
        headers: {
            "Authorization": 'Bearer sk_test_da1fd67380a1213f2bf1c9e09d4a033744eba0d8'
        }
    }, function (error, response, body) {
        if (error) {
            console.error("Error while communication with api and ERROR is :  " + error);
            res.send(error);
        }
        //console.log('body : ', body);
        var done = JSON.parse(body);
        console.log(done);
        res.send(done);

    })
};


exports.callback_url = function (req, res) {
    console.log("response headers" + JSON.stringify(req.headers));
    console.log("response url" + JSON.stringify(req.path));
    console.log("response body" + JSON.stringify(req.body));

}

exports.webhooks_paystack = function (req, res) {
    //var id = req.Userdata.email;

    //validate event
    // var secret = 'sk_test_da1fd67380a1213f2bf1c9e09d4a033744eba0d8';
    // var hash = crypto.createHmac('sha512', secret).update(JSON.stringify(req.body)).digest('hex');
    // if (hash == req.headers['x-paystack-signature']) {
    if (req.body.event === "charge.success") {
        console.log("the event " + req.body.event)
        res.send(200);

        const response = new Response({
            _id: new mongoose.Types.ObjectId(),
            reference: req.body.data.reference,
            status: req.body.data.status,
            amount: req.body.data.amount / 100,
            time: req.body.data.paid_at,
            chargestatus: req.body.event,
            authorizationcode: req.body.data.authorization.authorization_code,
            lastdigit: req.body.data.authorization.last4,
            exp_month: req.body.data.authorization.exp_month,
            exp_year: req.body.data.authorization.exp_year,
            card_type: req.body.data.authorization.card_type,
            email: req.body.data.customer.email,
            customerid: req.body.data.customer.id,
            subaccount: req.body.data.subaccount,
        })
        response
            .save()
            .then(result => {
                //console.log(result);
                //console.log("this is " + result.reference);

                console.log("Saved response");
                //todo:check if card exist, create if not
                var idc = result.lastdigit;
                var idmail = result.email;

                //console.log("this is " + idc);

                carddetails.count({lastdigit: idc, email: response.email}).exec().then(doc => {
                        //  req.userResponse = doc[0];
                        //console.log("this is l " + doc[0].lastdigit);


                        if (doc) {
                            console.log("card exist")
                            //res.send("card exist");
                        }
                        else {
                            const card = new carddetails({
                                _id: new mongoose.Types.ObjectId(),
                                //User: req.Userdata.userId,
                                customerid: result.customerid,
                                lastdigit: result.lastdigit,
                                exp_month: result.exp_month,
                                exp_year: result.exp_year,
                                card_type: result.card_type,
                                email: result.email,
                                authorizationcode: result.authorizationcode

                            })
                            card.save().then(cardi => {
                                console.log(cardi);
                                //console.log("this is " + cardi);

                                console.log("card saved");
                            })
                        }
                    }
                )
            })
            .catch(err =>
                console.log(err));
        res.status(200).json
    }
    else {
        res.sendStatus(status)
        res.send(400)

    }


}

exports.post_authorized = (req, res) => {
    var id = req.Userdata.email;
    //console.log("email " + id)

    carddetails.find({email: id}).exec().then(user => {
        req.userResponse = user[0];

        console.log("email " + req.userResponse.email)
        var options = {
            url: "https://api.paystack.co/transaction/charge_authorization",
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer sk_test_da1fd67380a1213f2bf1c9e09d4a033744eba0d8'
            },
            body: {


                "amount": req.body.amount * 100,
                "email": req.userResponse.email,
                "authorization_code": req.body.authorization_code,
                "subaccount": req.userResponse.subaccount,
            },
            json: true
        }

        request(options)
            .then(
                function (response) {
                    console.log(response)
                    res.send(response)
                })
            .catch(function (err) {
                res.send(err)
                console.log(err)

            })
    })
}

exports.list_subscription = (req, res) => {
    var id = req.Userdata.email;
    Plan.find({User: id}).exec().then(user => {
        req.userRes = user[0];
        console.log(req.userRes)
        res.status(200).json({
            ResponseCode: "200",
            message: user
        })
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            ResponseCode: "500",
            message: err
        })
    })
}
//}


