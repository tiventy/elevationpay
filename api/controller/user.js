const Users = require('../models/users');
var mongoose = require('mongoose');
const request = require('request-promise');
var nodemailer = require('nodemailer');


const jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt-nodejs');


exports.reset_passwords = (req, res) => {
    Users.findOne({ email: req.body.email}).select('username active email resettoken name').exec(function(err, user) {
        if (err) throw err; // Throw error if cannot connect
        if (!user) {
            res.json({ success: false, message: 'Username was not found' }); // Return error if username is not found in database
        }
        else if (!user.active) {
            res.json({ success: false, message: 'Account has not yet been activated' });
        }
         else {
            user.resettoken = jwt.sign({name: user.name,email: user.email }, process.env.JWT_TOKEN, { expiresIn: '24h' }); // Create a token for activating account through e-mail
            // Save token to user in database
            user.save(function(err) {
                if (err) {
                    res.json({ success: false, message: err }); // Return error if cannot connect
                } else {


                    var transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'tiventyglobal@gmail.com',
                            pass: '07032155156'
                        }
                    });


                    const mailOptions = {
                        from: 'hello@tiventy.com', // sender address
                        to: req.body.email, // list of receivers
                        subject: ' Elevation Pay', // Subject line
                        text: 'Hello ' + user.name + ', You recently request a password reset link. Please click on the link below to reset your password:<br><br><a href="' + req.hostUrl + '/reset/' + user.resettoken,
                        html: 'Hello<strong> ' + user.name + '</strong>,<br><br>You recently request a password reset link. Please click on the link below to reset your password:<br><br><a href="' + req.hostUrl + '/user/reset/' + user.resettoken + '">Reset Password</a>'
                    };
                    transporter.sendMail(mailOptions, function (err, info) {
                        if (err)
                            console.log(err)
                        else
                            console.log(info);
                    });
                    console.log(req.body.email);
                    //res.send("done " + req.body.email)
                    res.json({ success: true, message: 'Please check your e-mail for password reset link' }); // Return success message
                }
            });
        }
    });
};

exports.reset_passwordtoken = (req, res) => {
    Users.findOne({resettoken: req.params.token }).select().exec(function(err, user) {
        if (err) throw err; // Throw err if cannot connect
        var token = req.params.token; // Save user's token from parameters to variable
        // Function to verify token
        jwt.verify(token, process.env.JWT_TOKEN, function(err, decoded) {
            if (err) {
                res.json({ success: false, message: 'Password link has expired' }); // Token has expired or is invalid
            } else {
                if (!user) {
                    res.json({ success: false, message: 'Password link has expired' }); // Token is valid but not no user has that token anymore
                } else {
                    exports.save_password(req,res)
                    //res.json({ success: true, user: user }); // Return user object to controller
                }
            }
        });
    });
};

exports.save_password = (req, res) => {
    Users.findOne({ email: req.body.email }).select('username email name password resettoken').exec(function(err, user) {
        if (err) throw err; // Throw error if cannot connect
        if (req.body.password == null || req.body.password == '') {
            res.json({ success: false, message: 'Password not provided' });
        } else {
            user.password = req.body.password; // Save user's new password to the user object
            user.resettoken = false; // Clear user's resettoken
            // Save user's new data
            user.save(function(err) {
                if (err) {
                    res.json({ success: false, message: err });
                } else {
                    var transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'tiventyglobal@gmail.com',
                            pass: '07032155156'
                        }
                    });
                    const mailOptions = {
                        from: 'hello@tiventy.com', // sender address
                        //to: user.email, // list of receivers
                        subject: ' Elevation Pay', // Subject line
                        text: 'Hello ' + user.name + ', This e-mail is to notify you that your password was recently reset at '+req.hostUrl,
                        html: 'Hello<strong> ' + user.name + '</strong>,<br><br>This e-mail is to notify you that your password was recently reset at '+req.hostUrl};
                    transporter.sendMail(mailOptions, function (err, info) {
                        if (err)
                            console.log(err)
                        else
                            console.log(info);
                    });
                    console.log(req.body.email);
                    // Create e-mail object to send to user
                    res.json({ success: true, message: 'Password has been reset!' }); // Return success message
                }
            });
        }
    });
};

exports.user_get_all = (req, res, next) => {
    var mysort = { created: -1 };

    Users.find().sort(mysort)
    .select('user _id email created')
    .exec()
    .then(docs => {
      res.status(200).json(
        {
          ResponseCode: "200",
          count: docs.length,
          user: docs
        })
    }).catch(err => {
    res.status(500).json({
      ResponseCode: "500",
      message: err
    });
  });

};

exports.user_login = (req, res, next) => {
  var email = req.body.email;
  //var phone = req.body.phone;

  Users.find({email}).exec().then(user => {
    if (user.length < 1) {
      return res.status(404).json({
        ResponseCode: "422",
        message: "incorrect username or password"
      });
    }


      

    bcrypt.compare(req.body.password, user[0].password, (err, result) => {
      if (err) {
        return res.status(404).json({
          ResponseCode: "422",
          message: "incorrect username or password"

        });
      }
       // if (!user[0].active) {
       //      return res.json({ success: false, message: 'Account is not yet activated. Please check your e-mail for activation link.'}); // Account is not activated
       //  }

      if (result) {
        const token = jwt.sign({
                email:user[0].email,
            userId: user[0]._id,
            roles: user[0].roles
            }, process.env.JWT_TOKEN,
          {
            //expiresIn: "1h"
          });

        return res.status(200).json({
          ResponseCode: "200",
          message: "successful",
          token: token,
          user: user
        });

      }

      return res.status(404).json({
        ResponseCode: "401",
        message: "incorrect username or password"
      });
    })
  }).catch(err => {
    console.log(err);
    res.status(500).json({
      ResponseCode: "500",
      error: err
    });

  }

  )
    ;
};

exports.user_post = (req, res) => {
  if (req.file)
    console.log(req.file);

  Users.find({email: req.body.email}).exec().then(user => {
    if (user.length >= 1) {
      return res.status(200).json({
        ResponseCode: "422",
        message: "email exist"
      });
    }
    Users.find({phone: req.body.phone}).exec().then(create => {
      if (create.length >= 1) {
        return res.status(200).json({
          ResponseCode: "422",
          message: "phone number already exist"
        });
      } else {
        const user = new Users({
            _id: new mongoose.Types.ObjectId(),
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            phone: req.body.phone,
            roles: req.body.roles,
            active: req.body.active,
            temporarytoken : jwt.sign({email: req.body.email}, process.env.JWT_TOKEN, { expiresIn: '24h' })

        });
        user
          .save()
          .then(result => {
              var transporter = nodemailer.createTransport({
                  service: 'gmail',
                  auth: {
                      user: 'tiventyglobal@gmail.com',
                      pass: '07032155156'
                  }
              });


              const mailOptions = {
                  from: 'hello@tiventy.com', // sender address
                  to: user.email, // list of receivers
                  subject: ' Elevation Pay Activation link', // Subject line
                  text: 'Hello ' + user.name + ', thank you for registering at localhost.com. Please click on the following link to complete your activation: '+req.hostUrl+'/user/emailtoken/:token' + user.temporarytoken,
                  html: 'Hello<strong> ' + user.name + '</strong>,<br><br>Thank you for registering at localhost.com. Please click on the link below to complete your activation:<br><br><a href="'+req.hostUrl + '/user/emailtoken/' + user.temporarytoken + '">Click here</a>'
              };


              transporter.sendMail(mailOptions, function (err, info) {
                  if (err)
                      console.log(err)
                  else
                      console.log(info);
              });
              console.log(req.body.email);

            console.log(result);
            res.status(200).json({ResponseCode: "200", message: "Account registration successful please check email for verification link", createduser: user});
          })
          .catch(err => res.status(500).json({ResponseCode: "500", error: err, message: "registration not successful"}));
      }
    })
  });
};

exports.activate_emailtoken = (req, res) => {

    //console.log("here");
    Users.findOne({ temporarytoken: req.params.token }, function(err, user) {
        if (err) throw err;
        var token = req.params.token;

        // Function to verify the user's token
        jwt.verify(token, process.env.JWT_TOKEN, function(err, decoded) {
            if (err) {
                res.json({ success: false, message: 'Activation link has expired.' });
            } else if (!user) {
                res.json({ success: false, message: 'Activation link has expired.' });
            } else {
                user.temporarytoken = false;
                user.active = true;

                user.save(function(err) {
                    if (err) {
                        console.log(err);
                    } else {
                        var transporter = nodemailer.createTransport({
                            service: 'gmail',
                            auth: {
                                user: 'tiventyglobal@gmail.com',
                                pass: '07032155156'
                            }
                        });

                        // If save succeeds, create e-mail object
                        const mailOptions = {
                            from: 'hello@tiventy.com', // sender address
                            to: user.email,
                            subject: 'Localhost Account Activated',
                            text: 'Hello ' + user.name + ', Your account has been successfully activated!',
                            html: 'Hello<strong> ' + user.name + '</strong>,<br><br>Your account has been successfully activated!'
                        };

                        // Send e-mail object to user

                        transporter.sendMail(mailOptions, function (err, info) {
                            if (err)
                                console.log(err)
                            else
                                console.log(info);
                        });
                        console.log(req.body.email);

                        res.json({ success: true, message: 'Account activated!' });
                    }
                });
            }
        });
    });
};



exports.user_get_id = (req, res, next) => {
  var id = req.params.userid;

  Users.findById(id)
    .exec()
    .then(doc => {
      console.log(doc);
      res.status(200).json({
        ResponseCode: "200",
        doc: doc
      })
    }).catch(err => {
    console.log(err);
    res.status(500).json({ResponseCode: "500", error: err});
  });

};

exports.user_delete = (req, res, next) => {
  var id = req.params.userid;
  Users.remove({_id: id}).exec().then(doc => {
    res.status(200).json({
      ResponseCode: "200",
      message: "user deleted"
    })
  }).catch(err => {
    console.log(err);
    res.status(500).json({
      ResponseCode: "500",
      message: err
    })
  });

};

exports.user_put = (req, res, next) => {

  var id = req.params.id;

  Users.findByIdAndUpdate(id, {$set: req.body}).select('email name').then(user => {

    console.log(user);
    res.status(200).json({
      ResponseCode: "200",
      message: user
    })
  }).catch(error => res.status(500).json({
    ResponseCode: "500",
    message: error
  }));
};
exports.facebook = (req, res, next) => {
  //console.log("got here");
  console.log("req user", req.user)
};
