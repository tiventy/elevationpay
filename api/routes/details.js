var express = require('express');
var router = express.Router();
const CheckAuth = require('../middleware/checkauth');
const paycontroller = require('../controller/pay');
const acl = require('../middleware/acl');





router.post('/pay',CheckAuth,paycontroller.pay_api);
router.get('/getcard',CheckAuth,paycontroller.get_cards);
router.post('/checkusercard',CheckAuth,paycontroller.checkuser_card);
router.get('/listsubs',CheckAuth,paycontroller.list_subscription);



router.post('/newcard',CheckAuth,paycontroller.add_newcard);
router.post('/deletecard',CheckAuth,paycontroller.delete_card);


router.post('/mail',paycontroller.invite_friends);
router.post('/plan',CheckAuth,paycontroller.create_plan);
router.post('/subs',CheckAuth,paycontroller.Subscribe);
router.post('/cancelsubs',CheckAuth,paycontroller.Disable_Subscription);




router.get('/transactions',CheckAuth,paycontroller.get_transactions);
router.get('/getalltransactions',CheckAuth,acl.ensureRole("admin"),paycontroller.get_alltransactions);



router.post('/auth',CheckAuth,paycontroller.post_authorized);


router.get('/do',paycontroller.get_payapi);

router.post('/webhook',paycontroller.webhooks_paystack);
router.get('/callback', paycontroller.callback_url);


// router.get('/:detailsid',CheckAuth, detailscontroller.details_get_id);
//
// router.put('/:id/update',detailscontroller.details_put);
//
// router.delete('/:detailsid',CheckAuth, detailscontroller.details_delete);
//




module.exports = router;

