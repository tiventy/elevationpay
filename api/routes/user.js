var express = require('express');
 var router = express.Router();
 var  mongoose = require('mongoose');
 var bcrypt = require('bcrypt-nodejs');
 var multer = require('multer');
 const jwt = require('jsonwebtoken');
 const passport = require('passport');
 const facebookStrategy = require('passport-facebook').Strategy;
 const CheckAuth = require('../middleware/checkauth');

const acl = require('../middleware/acl');
 const upload = multer({dest: 'uploads/'});
 const usercontroller = require('../controller/user');

 const Users = require('../models/users');


router.put('/:id/update',CheckAuth,usercontroller.user_put);
router.get('/reset',usercontroller.reset_passwords);
router.get('/reset/:token',usercontroller.reset_passwordtoken);
router.put('/savepassword',usercontroller.save_password);
router.get('/emailtoken/:token',usercontroller.activate_emailtoken);




//router.get('/request',usercontroller.getrequest);

router.get('/getall',CheckAuth, acl.ensureRole("admin") ,usercontroller.user_get_all );

 router.post('/signup',usercontroller.user_post);

 router.post('/login',usercontroller.user_login);

 router.get('/:userid',CheckAuth,usercontroller.user_get_id);
 router.delete('/:userid',CheckAuth,usercontroller.user_delete);

 router.post('/facebook/auth',passport.authenticate('facebook'),CheckAuth,usercontroller.facebook);




module.exports = router;