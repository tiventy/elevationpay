
const logger = require('./logger');
const express = require('express');

const app = express();
app.logger = logger;

const morgan = require('morgan');
const bodyparser = require('body-parser');
const  mongoose = require('mongoose');
const passport = require('passport');
const facebook = require('passport-facebook').Strategy;
const jwt = require('jsonwebtoken');

//var mongoose = require('mongoose');
var url = 'mongodb://localhost:27017/Elevation-api';

mongoose.connect(url);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.on('open', function () {
  console.log('connected');

});


//mongoose.connect('mongodb+srv://sammy:' + process.env.MONGO_ATLAS_PW + '@tiventy-api-mc4p2.mongodb.net/test?retryWrites=true',{ useNewUrlParser: true } );


var userRoutes = require('./api/routes/user');
var payroutes = require('./api/routes/details');



app.use(morgan('dev'));
app.use(passport.initialize());
app.use((req,_,next)=>{ //loads all necessary details
  req.hostUrl = `${req.headers['x-forwarded-proto'] || req.protocol}://${req.get('host')}`;
  try {
    req.Userdata = jwt.verify(req.headers.authorization.split(" ")[1], process.env.JWT_TOKEN);
  } catch (error) {  }
  next();
});

app.use('/logs',express.static(app.logger.logPath),require('serve-index')(app.logger.logPath, { icons: true }));

//app.use('/uploadedevent',express.static('uploadedevent'));
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());



app.use('/user', userRoutes);
app.use('/details', payroutes);


app.all('*',(req, res) =>
{
  console.log(`Not found ${req.url}`);
  res.status(404).end('Not found');
});

app.use((error, req, res) =>
{
  console.log(error);
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
